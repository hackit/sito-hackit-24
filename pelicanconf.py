#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import json
import math
with open('hackmeeting.json') as buf:
    hm_metadata = json.load(buf)


# Non so dove metterla quindi le metto qui, poi vediamo

def geo_to_bbox(lat, lon, zoom):
    width = 425
    height = 350
    # m/pixel at zoom level 
    # see https://wiki.openstreetmap.org/wiki/Zoom_levels to change
    meters_pixel = 156543/(2**zoom)
    r_earth = 6371000
    
    dy = (width/2)*meters_pixel
    dx = (height/2)*meters_pixel
    latitude = lat
    longitude = lon
    lat_s  = latitude  + (dy / r_earth) * (180 / math.pi)
    lon_s  = longitude + (dx / r_earth) * (180 / math.pi) / math.cos(latitude * math.pi/180)
    lat_e  = latitude  - (dy / r_earth) * (180 / math.pi) 
    lon_e  = longitude - (dx / r_earth) * (180 / math.pi) / math.cos(latitude * math.pi/180)
    bbox = f"{lon_s}%2C{lat_s}%2C{lon_e}%2C{lat_e}"

    return bbox


# da un anno all'altro cambiare solo la variabile YEAR è sufficiente per le operazioni più base!
YEAR = hm_metadata['year']

EDITION = hex(YEAR - 1997)
AUTHOR = "Hackmeeting"
SITENAME = "Hackmeeting %s" % EDITION
CC_LICENSE = "by-nc-sa"
SITEURL = "/hackit%d" % (YEAR - 2000)

JINJA_GLOBALS = { 'hm': { 'edition': EDITION, **hm_metadata, 'osm': { 'bbox': geo_to_bbox(hm_metadata['location']['geo']['lat'], hm_metadata['location']['geo']['lon'], 18) } } }

PATH = "content"
PAGE_PATHS = ["pages"]
ARTICLE_PATHS = ["news"]
STATIC_PATHS = ["images", "talks", "extra"]
# DIRECT_TEMPLATES = ('search',)  # tipue search

TIMEZONE = "Europe/Paris"

DEFAULT_LANG = "it"

# Feed generation is usually not desired when developing
INDEX_SAVE_AS = "news.html"
ARTICLE_URL = "news/{slug}.html"
ARTICLE_SAVE_AS = "news/{slug}.html"
FEED_DOMAIN = "https://it.hackmeeting.org"
FEED_ALL_ATOM = "news.xml"
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None
# Social widget
SOCIAL = None
DEFAULT_PAGINATION = 10
USE_OPEN_GRAPH = False  # COL CAZZO

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DEFAULT_DATE = (YEAR, 3, 1)
TYPOGRIFY = True

PAGE_ORDER_BY = "navbar_sort"
PAGE_URL = "{slug}.html"
PAGE_SAVE_AS = "{slug}.html"
PAGE_LANG_URL = "{slug}.{lang}.html"
PAGE_LANG_SAVE_AS = "{slug}.{lang}.html"
BANNER = True
BANNER_ALL_PAGES = True
SITELOGO = "logo/logo.png"
# PAGE_BACKGROUND = 'images/background.jpg'
# THEME = 'themes/hackit0x15/'
THEME = "themes/to0x19/"
FAVICON = "images/cyberrights.png"
FONT_URL = "theme/css/anaheim.css"

# Custom css by sticazzi.
# CUSTOM_CSS = 'theme/css/hackit.css'
EXTRA_PATH_METADATA = {
    # 'extra/main.css': {'path': 'themes/pelican-bootstrap3/static/css/main.css' },
    "extra/favicon.png": {"path": "images/favicon.png"},
    "images/locandina.jpg": {"path": "images/locandina.jpg"},
}

# Pelican bootstrap 3 theme settings
BOOTSTRAP_THEME = "darkly"
HIDE_SITENAME = True
HIDE_SIDEBAR = True
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["langmenu", "talks", "tipue_search", "pelican_webassets", "pelican.plugins.jinja2content"]

# plugin/talks.py
SCHEDULEURL = "https://hackmeeting.org" + SITEURL + "/schedule.html"
TALKS_GRID_STEP = 15

MARKDOWN = {"extension_configs": {"markdown.extensions.toc": {}}}

