Anno nuovo sito nuovo
=========================

Come fare un sito per l'anno nuovo, abbastanza simile a quello vecchio?

copia ed aggiorna le date. Dove?

* `README.md`
* `content/pages/index.*` ci sono le date/luogo
* `content/pages/info*` c'è sicuramente roba da cambiare
* `hackmeeting.json` cambia il campo `year`
* `talks/meta.yaml`: cambia la startdate: deve essere quella del primo giorno da mettere in programma. Siccome a volte ci sono anche cose dal mercoledì, per prudenza indica il mercoledì come giorno di inizio.
