Stampa
#########

:slug: press
:navbar_sort: 9
:lang: it

Cartella stampa
""""""""""""""""""""""""""""""""""""""


Propaganda
"""""""""""""""""""""""""""""""""""""""

I materiali utili per la diffusione di hackmeeting {{hm.year}} sono nell'apposita pagina `Propaganda
<{filename}propaganda.md>`_


Foto
"""""""""""""""""""""""""""""""""""""""


.. image:: images/press/2016-IMG_0578.jpg
    :height: 200px
    :alt: La bacheca dei seminari dell'hackmeeting 2016, a Pisa
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0578.jpg

.. image:: images/press/2016-IMG_0581.jpg
    :height: 200px
    :alt: Sessione di elettronica digitale
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0581.jpg

.. image:: images/press/2016-IMG_0584.jpg
    :height: 200px
    :alt: Programmazione
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0584.jpg

.. image:: images/press/2016-IMG_0586.jpg
    :height: 200px
    :alt: Computer
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0586.jpg

.. image:: images/press/2016-IMG_0589.jpg
    :height: 200px
    :alt: Il LAN party: un posto dove sperimentare insieme
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0589.jpg

.. image:: images/press/2016-IMG_0574.jpg
    :height: 200px
    :alt: Un hack su Emilio: il famoso robottino degli anni '90 è stato riprogrammato
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0574.jpg


Comunicati stampa
"""""""""""""""""""""""""""""""""""""""




Comunicato Stampa 13 Giugno
=================================


| HACKMEETING
| 14-16 giugno 2024
| Centro Sociale Autogestito Magazzino 47
| Via Industriale 10, BRESCIA
|


Strumenti per la programmazione dei robot, scoprire i segreti dell'intelligenza artificiale, viaggiare nel cuore di un motore di ricerca, conoscere la storia della Radio antagonista bresciana, cucinare e conoscere la cucina vegana e palestinese, cantare sulle note dell' HTTPS. Sono solo alcuni dei temi al centro dell'Hackmeeting, incontro annuale delle controculture digitali che giunge nel 2024 alla sua ventisettesima edizione e che si svolge quest’anno a Brescia, al CSA Magazzino 47, in via Industriale 10, dal 14 al 16 giugno. Il Magazzino 47, sede del movimento antagonista dal 1985, e' strettamente legato all’emittente Radio Onda d’Urto, punto di riferimento della galassia antagonista bresciana, uno spazio autogestito dove l’impegno politico si intreccia con l’attività sociale, la cultura e la musica. Un festival (il programma è disponibile su www.hackmeeting.org) dell’insegnamento reciproco e dello scambio libero e gratuito, per scoprire come configurare server autogestiti a prova di privacy, con approfondimenti dal punto di vista tecnico e legale, ma anche reti digitali in assenza di internet o sistemi anticollisione per droni. Obiettivo: riportare la tecnologia sotto il controllo delle persone in un mondo in cui sempre di più sono le persone a finire sotto il dominio dei dispositivi, e cioè delle grandi aziende che ci stanno dietro. L’HackIT è solo per "hackers" con un' eccezzione diversa da quella comune, ovvero per chi vuole gestirsi la vita come preferisce e sa mettersi in gioco per farlo. Anche se non ha mai visto un computer in vita sua. 


Si parte dalle basi - l'utilizzo di un tester elettrico o l'installazione di Linux sul proprio computer - per lanciarsi verso la conquista del controllo della propria indipendenza digitale. Seminari aperti a tutte le persone e gratuiti, per mettere insieme idee e competenze. Una formula consolidata, figlia di un'idea che precorse i tempi. La prima edizione risale al 1998, in oltre un quarto di secolo, l'Hackmeeting ha accompagnato tutte le fasi della rete. Lanciando l'allarme con anni di anticipo su quello che sarebbe successo in Italia e nel mondo. La voracità di Google sui dati delle persone, la pervasività dei social e le dipendenze che hanno provocato, il controllo sociale attraverso i dispositivi, le bolle digitali fatte di propaganda personalizzata, la trasformazione delle criptovalute da strumenti di libertà a mezzi speculativi, la fine della neutralità della rete, le insidie del telefonino nei rapporti di lavoro o come strumento di sorveglianza.

Ogni anno, la comunità dell'Hackmeeting si riunisce in una città diversa e, tenacemente, continua a interrogarsi sulle trasformazioni che la tecnologia sta impoendo al mondo, alla ricerca di strade alternative. Fatte di meno dominio e controllo; e di più autonomia, condivisione e consapevolezza.
