Title: Come arrivare
slug: come-arrivare
navbar_sort: 2
lang: it

Hackit {{hm.year}} si svolgerà al [{{hm.location.name}}]({{hm.location.website}}), in [{{hm.location.address}}, {{hm.location.city}}](https://www.openstreetmap.org/#map=16/{{hm.location.geo.lat}}/{{hm.location.geo.lon}})

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox={{hm.osm.bbox}}&amp;layer=mapnik&amp;marker={{hm.location.geo.lat}}%2C{{hm.location.geo.lon}}" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat={{hm.location.geo.lat}}&amp;mlon={{hm.location.geo.lon}}#map=18/{{hm.location.geo.lat}}/{{hm.location.geo.lon}}" target="_blank">Visualizza Mappa</a></small>

Il centro sociale è facilmente raggiungibile. Se vi disorientate cercate il cimitero monumentale di Brescia che è a fianco del Magazzino 47.

## Con i piedi
Saprai come fare, ne siamo certi. Da dove parti parti.

## Con il treno e l'autobus

Lo spazio dista circa 20 min a piedi dalla stazione dei treni e dei bus di Brescia.
Fuori dalla stazione dei treni è possibile prendere l'autobus numero 3 direzione Mandolossa e il 9 in direzione Violino con fermata in via Milano subito dopo il cimitero.


## Con veicoli a motore

Il Magazzino 47 è facilmente raggiungibile dall'autostrada A4, uscita Brescia Ovest.
Intorno allo spazio sono presenti diversi parcheggi gratuiti con possibilità di posteggiare camper e furgoni in diverse aree e vie adiacenti anche se siamo in città.


## Con l'aereo

L'aeroporto più servito e più vicino è quello di Milano Bergamo (BGY), distante circa 40km da Brescia.
Sono disponibili autobus autostradali che collegano l'aeroporto con la stazione di Brescia in circa un'ora al costo di 12€.
Per raggiungere Brescia, è anche possibile passare prima per Bergamo con gli autobus urbani (3€) e poi prendere il treno (5.20€), ma i tempi si allungano di altri trenta minuti.
