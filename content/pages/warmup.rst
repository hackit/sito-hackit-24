Warmup
======

:slug: warmup
:navbar_sort: 5
:lang: it

Cosa sono
"""""""""""""""""""""""""""""""""""""""

I warmup sono eventi "preparatori" ad hackmeeting. Avvengono in giro per l'Italia, e possono trattare gli argomenti più disparati.

Proporre un warmup
"""""""""""""""""""""""""""""""""""""""

Vuoi fare un warmup? ottimo!

* iscriviti alla `mailing list di hackmeeting <https://www.autistici.org/mailman/listinfo/hackmeeting>`_.
* scrivi in mailing list riguardo al tuo warmup: non c'è bisogno di alcuna "approvazione ufficiale", ma segnalarlo in lista è comunque un passaggio utile per favorire dibattito e comunicazione.


Elenco
"""""""""""""""""""""""""""""""""""""""
.. contents:: :local:

==========================

Fighting Technologies of Domination: From decentralised event platforms to the right to analogue
-------------------------------------------------------------------------------------------------

Domenica 9 giugno @ Disruption Network Lab (Berlino)

https://www.disruptionlab.org/event/fighting-technologies-of-domination

Si discuterà del diritto a vivere senza smartphone e di software fuori dal radar GAFAM con il collettivo Balotta.org di Bologna che condurrà un laboratorio su Gancio. Oltre a balotta ci sarà Agnese del collettivo [C.I.R.C.E.](https://circex.org) a moderare e Roberta Barone che approfondirà il tema del diritto a non avere uno smartphone mostrando alcune clip dal documentario ["Digital Dissidents"](https://digitalnisvobody.cz/wp-content/uploads/2022/09/Digitalni-disidenti_ABOUT-FILM.pdf). [Qui il trailer](https://iteroni.com/watch?v=qd9VGM4Q3WI).

* 15:00-16:30: Open discussion – From decentralised platforms to the right to analogue.

The first part of the meetup will be dedicated to an open discussion, on the one hand to name the contradictions of the acritical adoption of smartphones and to give a voice and a framework to doubts, questions and demands that remain largely unspoken, and on the other hand to explore the possibilities of grassroots technologies and decentralised open-source software to share events and collect experiences beyond the use of social media platforms.

* 16:45-18:15: Gancio and Balotta.org – Exploring open-source software for event publishing.

After a short break, the second part of the meetup will be hosted by the Balotta Collective (Italy) and we will delve deeper into the exploration of Gancio.

Born in Turin (Italy) out of political hacking movements, Gancio is a free open-source software designed for event publishing.



Oooh issaa! ...ooooh issaaa! il MIAI salpa!
--------------------------------------------

Naviganti! dal primo marzo scioglieremo le vele e tireremo su l’ancora. Si riparte con l’allestimento del MIAI! Fino ad ora abbiamo pulito, ristrutturato, ammobiliato, allestito parte della Biblioteca e realizzato un affresco :) Si è fatta ora di affrontare la massa. La quantità di materia informatica che attende ormai da troppo tempo di essere liberata dal suo involucro di plastica e cartone per ritornare alla luce nel pieno del suo splendore. I pallet sono lì...intombati...aspettano soltanto noi!

Grazie anche ad Hackmeeting abbiamo resistito.
Questo museo è frutto anche di questa comunità.

per chiarezza, i primi fine settimana dei prossimi mesi sono piccoli Warm Up di costruzione del MIAI in attesa dell'appuntamento Hackmeeting a Brescia :)

E' l'occasione per scoprire i reperti, familiarizzare, prendersene cura, riaccenderli.

* Ecco le date dei venerdì-sabato-domenica:

1-2-3 marzo

5-6-7 aprile

3-4-5 maggio

31 maggio 1-2 giugno

5-6-7 luglio inizia il Cool Down con sessione di restauro del GE-120 da tenersi il 26-27-28 ultimo fine settimana di luglio.

Questi sono appuntamenti fissi, ma se qualcun in transito volesse fermarsi in date diverse, male non fa!

Per informazioni su ospitalità e altro scrivete a info@verdebinario.org

Ti invitiamo a salire a bordo e a dare una mano in questa impresa! 

Obiettivo?

Allestire il Museo Interattivo di Archeologia Informatica - MIAI.

Quando?

Ogni primo fine settimana del mese da marzo a luglio, qui al MIAI, in Calabria, a Rende (CS) -Via C.B.Cavour 4.

Ospitalità?

Provate a organizzarvi e nel caso vogliate un supporto contattateci in privato.

E se non posso venire?

Siamo liet* anche di un piccolo/grande contributo monetario che è utile per affrontare gli imprevisti di un allestimento di questo genere

E alla fine?

Cool Down! Dopo Hackmeeting 2024, una sessione di restauro del GE-120 da tenersi il 26-27-28 ultimo fine settimana di luglio!

E alla fine fine?

Arrivare in porto, attraccare e vivere il MIAI funzionante!


Museo Interattivo di Archeologia Informatica - MIAI https://miai.musif.eu/



Questo titolo non è autogenerato
---------------------------------

Martedì 28 maggio @ via Mascarella 59A ZEM GARDEN (Bologna)

https://balotta.org/event/questo-titolo-non-e-autogenerato

Tranquillu il tuo lavoro da titolista sopravviverà alle AI.

Una serata dedicata al futuro dell'intelligenza artificiale insieme al collettivo Hacklabbo.



RADIO EUSTACHIO: due giorni di incontri, costruzioni, accrocchi e musica!
--------------------------------------------------------------------------

Sabartolomenica 25 e 26 maggio @ Gigi Piccoli (Verona)

https://lasitua.org/event/radio-eustachio-due-giorni-di-incontri-costruzioni-accrocchi-e-musica


**sabato 25/05**

* ore 11.00: Come funziona lo strumento radio: onde medie e corte

* ore 15.00: Calendari condivisi. Presentazione di Gancio e le istanze gancio.cisti.org, balotta.org e lasitua.org

* ore 17.00: Autocostruzione di un ricevitore AM (o qualcosa di simile)

* ore 19.00: Concerto con i Kani Sholty e a seguire selecta musicale con djset eustachiani!

**domenica 26/05**

* ore 11.00: Mercato autoproduzioni del Gigi Piccoli con diretta a microfono aperto

* ore 15.00: Chiacchiera su Gancio: una proposta per un calendario condiviso locale

* ore 17.00: Concerto e streaming di DucalicRock! con The Slurmies e Maybe Wonders

Radio Eustachio ha un blog: https://eustachio.indivia.net/blog

Il tutto si svolgerà al Gigi Piccoli, in via Caroto 1, Verona



Presentazione A-K-M-E, astro cyber crypto bluff
------------------------------------------------

Sabato 25 maggio @ Piano Terra Lab (Milano)

* ore 19.30 cibo per tutti i palati

* ore 20.30 presentazione dei libri Cyber bluff e Crypto bluff con l’autore ginox

Cyber bluff, Collana BookBlock, Eris edizioni

Sono poche le persone che si interrogano sulla rete a cui questi servizi si appoggiano per capire da dove viene, com’è nata e da chi è controllata. Sappiamo come funziona questo strumento, questa rete che è onnipresente nelle nostre vite? Sappiamo dove vanno i nostri dati e chi ne trae profitto? Abbiamo davvero bisogno di comunicazioni istantanee? Internet è davvero democratico? Il libro descrive i servizi online più diffusi, raccontandone la genesi, il funzionamento, e fornendo alcuni consigli utili per utilizzarli in maniera più consapevole.

Crypto bluff, Collana BookBlock, Eris edizioni

Bitcoin e NFT, blockchain e miner: questi termini provocano reazioni contrastanti. Da un lato suscitano un forte hype, dall’altro appaiono come l’ennesima bolla finanziaria speculativa, neppure troppo raffinata. Non si tratta di un libro su come svoltare. L’obiettivo è spogliare le criptovalute dal glamour che le circonda e spiegare come la componente rivoluzionaria e libertaria di cui si fregiano sia solo una strategia di marketing.

* ore 22:00 Acaroroscopo, ovvero l’oroscopo che non ti aspettavi! Musica live e testi a cura di Lobo

Cosa accade quando un hacker decide di darsi all’astrologia? Vieni a scoprire finalmente cosa dicono le stelle del tuo benessere digitale e della tua privacy. Con l’Acaroroscopo, la tua guida all’hacking astrale, i misteri informatici del tuo destino non avranno piu’ segreti!

* ore 23:00 Low-tech live di Pablito el drito

25 maggio a partire dalle 19:30

https://akme.vado.li 

https://www.pianoterralab.org/events/a-k-m-e-astro-cyber/



Assemblea di istanza mastodon bida.im e pranzo di autofinanziamento
-------------------------------------------------------------------

Domenica 19 maggio 2020 dalle 12.00 @ Spazio Libero Autogestito @Vag61 (in via Paolo Fabbri 110 a Bologna)

https://bida.im/news/quarta-assemblea-generale-distanza-mastodon-bida-im/



Presentazione del libro Cyber Bluff (Catania)
----------------------------------------------

Sabato 18 maggio @ Laboratorio Urbano Popolare Occupato (in Piazza Pietro Lupo 25, Catania)

Presentazione del libro Cyber Bluff: storie rischi e vantaggi della rete per navigare consapevolmente (2021, eris). Discuteremo e approfondiremo insieme all'autore Ginox dei temi trattati nel libro.

https://www.attoppa.it/event/presentazione-di-cyber-bluff



Presentazione Antennine/Ninux in appennino + cena benefit
----------------------------------------------------------

Lunedì 13 Maggio @ Circolo Anarchico Berneri, Bologna (BO)

https://circoloberneri.indivia.net/events/event/presentazione-del-progetto-antennine-in-appennino-bolognese-e-cena-di-autofinanziamento



Brugole e merletti
-------------------

10-11-12 maggio @ NextEmerson Via di Bellagio 15 - Firenze

https://doityourtrash.noblogs.org/



Instant Messaging e sicurezza
------------------------------

10 maggio a partire dalle 21:00 @ Pianoterra Lab, Milano
https://www.pianoterralab.org/events/a-k-m-e-instant-messaging/

Sono cambiati i mezzi con i quali comunichiamo: sempre meno si usano email, sempre di più le app da cellulare, in particolare, per la messaggistica istantanea. L’Instant Messaging è diventato il sistema di comunicazione più diffuso, anche nell’organizzazione del lavoro. Usati spesso da attivist* e agitator* a causa della loro enorme diffusione, questi sistemi immediatamente disponibili sono diventati un obbiettivo appetibile. La vulnerabilità di queste applicazioni può così mettere a rischio la libertà, la privacy e la sicurezza delle persone. Crittografia asimmetrica, metadati, immagini, backup, codice sorgente, ne discutiamo in una tavola rotonda allargata sulla base di strategie e alternative disponibili.

con samba – underscore hacklab (Torino)

10 maggio a partire dalle 21:00

A-K-M-E

https://akme.vado.li



Workshop mappe dalla carta al digitale
---------------------------------------

Martedì 9 aprile @ Vag 61, via Paolo Fabbri 110, Bologna (BO)

https://ciemmona.noblogs.org/workshop-mappe-dalla-carta-al-digitale/
