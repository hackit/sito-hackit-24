Schedule
===========

:slug: schedule
:navbar_sort: 3
:lang: en

.. `Add the schedule <schedule.ics>`_ |ics| as a calendar

.. |ics| image:: {attach}/images/ics.png
   :height: 2em
   :target: webcals://it.hackmeeting.org/schedule.ics

To track the schedule, you might want to:

 * `add it to Android <https://ggt.gaa.st/#url=https://it.hackmeeting.org/schedule.ics>`_
 * `add it to your desktop calendar <webcals://it.hackmeeting.org/schedule.ics>`_ (ie: Thunderbird)
 * `raw URL <https://it.hackmeeting.org/schedule.ics>`_

The schedule is still work in progress: a large part of hackmeeting
contents are scheduled last-minute! We will **stop** updating this page on Thursday, 23:00. From that time,
the only authoritative source will be the paper-made schedule in the LAN space.

Read the `call for contents <{filename}call.en.md>`_ and propose yours in `mailing list <{filename}contatti.rst>`_.


Contents in a language other than Italian are not only accepted, but
appreciated!

Hackmeeting (still) hasn't a proper translation system, but you can
find a bunch of people to ask to do translations when you need it.

.. raw:: html

    To see the map, <a href="{static}/images/mappa_hm23.png" target="_blank" rel="noopener noreferrer">click here</a>


.. talkgrid::
    :lang: en

.. talklist::
    :lang: en
